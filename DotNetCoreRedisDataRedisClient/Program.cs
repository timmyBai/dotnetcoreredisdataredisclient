using DotNetCoreRedisDataRedisClient.Utils;


public class Program
{
    private static DotNetCoreRedisConnection? redis = null;

    public static void Main()
    {
        try
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                string role = "管理者";

                redis.SetString("role", role);

                redis.DeleteKey("pc");
                redis.DeleteKey("role");

                Console.WriteLine("redis set value success");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            redis.Close();
            redis.Dispose();
        }

        Console.ReadKey();
    }
}

