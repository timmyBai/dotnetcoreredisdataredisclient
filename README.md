# DotNetCoreRedisDataRedisClient

<div>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet/8.0" target="_blank">
        <img src="https://img.shields.io/badge/C%23-8.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet/8.0" target="_blank">
        <img src="https://img.shields.io/badge/.NetCore-8.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet/8.0" target="_blank">
        <img src="https://img.shields.io/badge/ConsoleApplication-8.0.0-blue">
    </a>
    <a href="https://redis.io/" target="_blank">
        <img src="https://img.shields.io/badge/Redis-latest-blue">
    </a>
</div>

## 簡介

DotNetCoreRedisDataRedisClient 是一個建構共用連線 Redis 資料庫 架構解決方案，他基於 C#、.Net Core 8 實現，他使用最新 Redis 資料庫，內置共用連線方法、資料庫語法執行，跨平台相容性，輕量級、高效能且模組化，提供了許多資料庫處理方法，他可以幫助你快速搭建 Redis 資料類型，可以幫助你解決許多需求。

## 功能

```tex
- 初始化連線
- 檢查 redis 連線狀態
- 釋放資料庫資源
- 關閉資料庫
- 取得 redis 資料庫索引
- 設定 redis 鍵值 json 字串格式
- 取得 redis 鍵值 json 字串格式
- 設定 redis 鍵值字串格式
- 取得 redis 鍵值字串格式
- 刪除 redis 鍵值
- 取得 redis 所有鍵值
- 設定 hash 鍵值
- 設定 hash 鍵值支援 hash entry
- 設定 hash 鍵值支援 json
- 取得 hash 鍵值裡面單一個鍵值內容
- 取得 hash 鍵值裡面 json 轉換內容
- 取得 hash 鍵值裡面全部鍵值及數值
- 刪除 hash 鍵值內部鍵值
- 取得 hash 鍵值長度
```

## 開發

```git
git clone https://gitlab.com/timmyBai/DotNetCoreRedisDataRedisClient
```

## 測試

```tex
dotnet test DotNetCoreRedisDataRedisClient.Tests
```

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |