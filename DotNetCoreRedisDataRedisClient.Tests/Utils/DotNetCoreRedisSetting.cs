namespace DotNetCoreRedisDataRedisClient.Tests.Utils
{
    public class DotNetCoreRedisSetting
    {
        /// <summary>
        /// 主機位置
        /// </summary>
        public string Host = "localhost";

        /// <summary>
        /// 連線 port 號
        /// </summary>
        public int Port = 6379;

        public string GetRedisConnectionString()
        {
            return $@"{Host}:{Port}";
        }
    }
}
