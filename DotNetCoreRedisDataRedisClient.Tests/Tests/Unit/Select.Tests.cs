using DotNetCoreRedisDataRedisClient.Tests.Model;
using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit
{
    [TestClass]
    public class SelectTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試 redis 取得所有 key
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestGetAllKeys()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                var keys = redis.GetAllKeys();

                Assert.IsTrue(keys.Length > 0);
            }
        }
    }
}
