using DotNetCoreRedisDataRedisClient.Tests.Model;
using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit.StringType
{
    [TestClass]
    public class StringTypeInsertTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試 redis 塞入 json
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestSetStringJson()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                StudentModels studentModel = new StudentModels()
                {
                    name = "tim",
                    age = 12
                };

                redis.SetStringJson("student", studentModel);

                StudentModels model = redis.GetStringJson<StudentModels>("student");

                Assert.IsTrue(model != null);
            };
        }

        /// <summary>
        /// 測試 redis 設定字串
        /// </summary>
        [Test]
        [Order(2)]
        [TestMethod]
        public void TestSetString()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                string role = "管理者";

                redis.SetString("role", role);
                string getRole = redis.GetString("role");

                Assert.IsTrue(!string.IsNullOrEmpty(getRole));
            }
        }
    }
}
