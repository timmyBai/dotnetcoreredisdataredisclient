using DotNetCoreRedisDataRedisClient.Tests.Model;
using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit.StringType
{
    [TestClass]
    public class StringTypeSelectTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試 redis 取得 json
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestGetStringJson()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                StudentModels studentModel = redis.GetStringJson<StudentModels>("student");

                CarModels carModel = redis.GetStringJson<CarModels>("car");

                Assert.IsTrue(studentModel != null);
                Assert.IsTrue(carModel == null);
            }
        }

        /// <summary>
        /// 測試 redis 取得字串
        /// </summary>
        [Test]
        [Order(2)]
        [TestMethod]
        public void TestGetString()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                string getRole = redis.GetString("role");
                string gender = redis.GetString("gender");

                Assert.IsTrue(!string.IsNullOrEmpty(getRole));
                Assert.IsTrue(string.IsNullOrEmpty(gender));
            }
        }
    }
}
