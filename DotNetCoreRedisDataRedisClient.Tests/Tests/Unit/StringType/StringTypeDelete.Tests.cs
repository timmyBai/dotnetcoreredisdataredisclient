using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit.StringType
{
    [TestClass]
    public class StringTypeDeleteTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試 redis 刪除 key
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestDeleteStringKey()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                redis.DeleteKey("role");

                string role = redis.GetString("role");

                Assert.IsTrue(string.IsNullOrEmpty(role));
            }
        }
    }
}
