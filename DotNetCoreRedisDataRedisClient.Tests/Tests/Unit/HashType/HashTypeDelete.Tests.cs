using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit.HashType
{
    [TestClass]
    public class HashTypeDeleteTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試 redis 刪除 key
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestDeleteHash()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                redis.DeleteHashKey("school", "name");

                string name = redis.GetHash("school", "name");

                Assert.IsTrue(string.IsNullOrEmpty(name));
            }
        }
    }
}
