using DotNetCoreRedisDataRedisClient.Tests.Model;
using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StackExchange.Redis;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit.HashType
{
    [TestClass]
    public class HashTypeInsertTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試設定 hash 鍵值支援 json
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestSetHashJson()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                ComputerModels computer = new ComputerModels();
                computer.cpu = "intel i9 14900";
                computer.harddisk = "ADATA SU900 1 TB";

                redis.SetHashJson("pc", "components", computer);

                var pc = redis.GetHashJson<ComputerModels>("pc", "components");

                Assert.IsTrue(pc != null);
            }
        }

        /// <summary>
        /// 測試設定 hash key
        /// </summary>
        [Test]
        [Order(2)]
        [TestMethod]
        public void TestSetHash()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                redis.SetHash("computer", "cpu", "intel i9 14900");
                redis.SetHash("computer", "harddisk", "ADATA SU900 1 TB");

                var computerCpu = redis.GetHash("computer", "cpu");

                Assert.IsTrue(computerCpu != null);
            }
        }

        /// <summary>
        /// 測試設定 hash key for HashEntry
        /// </summary>
        [Test]
        [Order(3)]
        [TestMethod]
        public void TestSetHashForHashEntry()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                HashEntry[] entry = new HashEntry[] {
                    new HashEntry("name", "測試一大學"),
                    new HashEntry("department", "資訊管理系")
                };

                redis.SetHash("school", entry);

                RedisValue? schoolDepartment = redis.GetHash("shell", "department");

                Assert.IsTrue(schoolDepartment != null);
            }
        }
    }
}
