using DotNetCoreRedisDataRedisClient.Tests.Model;
using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit.HashType
{
    [TestClass]
    public class HashTypeSelectTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試取得 get hash json 
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestGetHashJson()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                var keys = redis.GetHashJson<ComputerModels>("pc", "components");

                Assert.IsTrue(keys != null);
            }
        }

        /// <summary>
        /// 測試取得 hash key
        /// </summary>
        [Test]
        [Order(2)]
        [TestMethod]
        public void TestGetHash()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                var schoolName = redis.GetHash("school", "name");

                Assert.IsTrue(schoolName != null);
            }
        }

        /// <summary>
        /// 測試取得 hash 長度
        /// </summary>
        [Test]
        [Order(3)]
        [TestMethod]
        public void TestHashLength()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                long count = redis.HashLength("school");

                Assert.IsTrue(count > 0);
            }
        }

        /// <summary>
        /// 測試取得 hash 所有 key 及 value
        /// </summary>
        [Test]
        [Order(4)]
        [TestMethod]
        public void TestGetHashAll()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();
                redis.GetDatabase(0);

                var keys = redis.GetHashAll("school");

                Assert.IsTrue(keys.Length > 0);
            }
        }
    }
}
