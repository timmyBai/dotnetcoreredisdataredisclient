using DotNetCoreRedisDataRedisClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreRedisDataRedisClient.Tests.Tests.Unit
{
    [TestClass]
    public class ConnectTests
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 測試 redis 連線
        /// </summary>
        [Test]
        [Order(1)]
        [TestMethod]
        public void TestRedisConnectSuccess()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();

                Assert.IsTrue(redis.Status());
            }
        }

        /// <summary>
        /// 測試 redis 連線關閉
        /// </summary>
        [Test]
        [Order(2)]
        [TestMethod]
        public void TestRedisConnectClose()
        {
            using (redis = new DotNetCoreRedisConnection())
            {
                redis.Connect();

                redis.Close();
                redis.Dispose();

                Assert.IsTrue(redis.Status() == false);
            }
        }
    }
}
