namespace DotNetCoreRedisDataRedisClient.Tests.Model
{
    public class CarModels
    {
        /// <summary>
        /// 車款
        /// </summary>
        public string type { get; set; } = "YARis CROSS";

        /// <summary>
        /// 車子顏色
        /// </summary>
        public string color { get; set; } = "white";

        /// <summary>
        /// 引擎
        /// </summary>
        public string engine { get; set; } = "Dual VVT-i";

        /// <summary>
        /// 煞車系統
        /// </summary>
        public string brakingSystem { get; set; } = "ABS";

        /// <summary>
        /// 頭燈
        /// </summary>
        public string headlamp { get; set; } = "LED";
    }
}
