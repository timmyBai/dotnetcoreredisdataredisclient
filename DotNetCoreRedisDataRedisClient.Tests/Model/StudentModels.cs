namespace DotNetCoreRedisDataRedisClient.Tests.Model
{
    /// <summary>
    /// 學生資料模型
    /// </summary>
    public class StudentModels
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; } = "";
        
        /// <summary>
        /// 年齡
        /// </summary>
        public int age { get; set; } = 0;
    }
}
