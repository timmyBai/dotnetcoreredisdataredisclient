namespace DotNetCoreRedisDataRedisClient.Tests.Model
{
    /// <summary>
    /// 電腦模型
    /// </summary>
    public class ComputerModels
    {
        /// <summary>
        /// 中央處理器
        /// </summary>
        public string cpu { get; set; } = "";

        /// <summary>
        /// 硬碟
        /// </summary>
        public string harddisk { get; set; } = "";
    }
}
